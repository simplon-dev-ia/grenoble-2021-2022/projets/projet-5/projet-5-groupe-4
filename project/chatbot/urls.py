from django.urls import path
from . import views

urlpatterns = [
    path('', views.get_request, name='get_request'),
    path('save_url_article', views.save_url_article, name='save_url_article'),
    path('save_url_video', views.save_url_video, name='save_url_video'),


]

