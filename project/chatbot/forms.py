from django import forms
from .models import Article, Video


class RequestForm(forms.Form):
    init_msg = "Entrez votre requête ici"
    request_text = forms.CharField(label='', max_length=500, initial=init_msg, required=False)


class SaveArticleUrlForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['url_article'].required = False

    class Meta:
        model = Article
        fields = ['url_article', 'key_word']
        labels = {"url_article": ""}
        exclude = ('key_word',)
        init_msg = "Coller une url ici"


class SaveVideoUrlForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['url_video'].required = False

    class Meta:
        model = Video
        fields = ['url_video', 'key_word']
        labels = {"url_video": ""}
        exclude = ('key_word',)
        init_msg = "Coller une url ici"
