from django.contrib import admin
from .models import Video, Article, KeyWord

admin.site.register([Video, Article, KeyWord])

# Register your models here.
