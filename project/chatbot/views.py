from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from .forms import RequestForm, SaveArticleUrlForm, SaveVideoUrlForm
from .modules.context_manager import intent_manage
from .models import Article, Video, KeyWord
from .modules.extract_meta import extract_kw, extract_video_kw
import requests


# Create your views here.

def get_request(request):
    luis_endpoint = "https://luisaragrenoble.cognitiveservices.azure.com/luis/prediction/v3.0/apps/4458ce7e-f128-47b6-9cd3-a6ef596ead2d/slots/staging/predict?verbose=true&show-all-intents=true&log=true&subscription-key=bba66aa8e81347349a2bed28a28981d2&query="
    chatbot_pos = "Rechercher"
    resultat_pos = 'Liste des résultats'

    if request.method == 'POST':
        request_form = RequestForm(request.POST)
        save_url_article_form = SaveArticleUrlForm(request.POST)
        save_url_video_form = SaveVideoUrlForm(request.POST)
        if request_form.is_valid():
            query_set_list = []
            submitted_request = request_form.cleaned_data['request_text']
            r = requests.get(luis_endpoint+submitted_request).json()
            intent_context = intent_manage(r, save_url_article_form,
                                           request_form,
                                           save_url_video_form,
                                           submitted_request, chatbot_pos, resultat_pos, query_set_list)

            article_query_set_list = []
            video_query_set_list = []
            key_word_db = []
            kw_searched = intent_context["subjects"].lower().split()

            for kw in KeyWord.objects.all():
                key_word_db.append(kw.key_word)

            if intent_context['top_intent'] == 'rechercher mots clés':
                print(f"top intent is: {intent_context['top_intent']}")

            elif intent_context['top_intent'] == 'Lire article':

                print("------------ARTICLE------------------------------------")
                print(f"top intent is: {intent_context['top_intent']}")
                print(f"main subject is: {intent_context['subjects']}")
                print(f"kw_searched: {kw_searched}")

                for kw in kw_searched:
                    if kw in key_word_db:
                        article_query_set = Article.objects.filter(key_word__key_word=kw)
                        for e in article_query_set:
                            if e.url_article not in article_query_set_list:
                                article_query_set_list.append(e.url_article)
                                query_set_list.append(e.url_article)
                print(f"kw_db : {kw}")
                print(f"query set list : {query_set_list}")
                print(type(query_set_list))

            elif intent_context['top_intent'] == 'Regarder vidéo':

                print("---------------VIDEO---------------------------------")
                print(f"top intent is: {intent_context['top_intent']}")
                print(f"main subject is: {intent_context['subjects']}")
                print(f"kw_searched: {kw_searched}")

                for kw in kw_searched:
                    if kw in key_word_db:
                        video_query_set = Video.objects.filter(key_word__key_word=kw)
                        for e in video_query_set:
                            if e.url_video not in video_query_set_list:
                                video_query_set_list.append(e.url_video)
                                query_set_list.append(e.url_video)
                print(f"kw_db : {kw}")
                print(f"query set list : {query_set_list}")

            return render(request, 'chatbot/formulaire.html', intent_context)

    else:
        request_form = RequestForm()
        save_url_article_form = SaveArticleUrlForm(initial={"url_article": "Coller une url ici"})
        save_url_video_form = SaveVideoUrlForm(initial={"url_video": "Coller une url ici"})
        context = {'resultat': resultat_pos, 'chatbot_pos': chatbot_pos,
                   'request_form': request_form, 'save_url_article_form': save_url_article_form, 'save_url_video_form': save_url_video_form}

        return render(request, 'chatbot/formulaire.html', context)


def save_url_article(request):
    chatbot_pos = "Rechercher"
    resultat_pos = 'Liste des résultats'
    if request.method == 'POST':
        save_url_article_form = SaveArticleUrlForm(request.POST)
        save_url_video_form = SaveVideoUrlForm(request.POST)
        request_form = RequestForm(request.POST)
        if save_url_article_form.is_valid():
            new_url = save_url_article_form.cleaned_data['url_article']
            if not new_url == "":
                new_article = save_url_article_form.save()
                title_kw = extract_kw(new_article)
                for kw in title_kw:
                    kw_db = KeyWord(key_word=kw)
                    kw_db.save()
                    new_article.key_word.add(kw_db)

            save_url_article_form = SaveArticleUrlForm(initial={"url_article": "Coller une url ici"})
            save_url_video_form = SaveVideoUrlForm(initial={"url_video": "Coller une url ici"})
            request_form = RequestForm()
            context = {'chatbot_pos': chatbot_pos, 'save_url_article_form': save_url_article_form,
                       'request_form': request_form,
                       'save_url_video_form': save_url_video_form}
            return render(request, 'chatbot/formulaire.html', context)


def save_url_video(request):
    chatbot_pos = "Rechercher"
    resultat_pos = 'Liste des résultats'
    if request.method == 'POST':
        save_url_article_form = SaveArticleUrlForm(request.POST)
        save_url_video_form = SaveVideoUrlForm(request.POST)
        request_form = RequestForm(request.POST)
        if save_url_video_form.is_valid():
            new_url = save_url_video_form.cleaned_data['url_video']
            if not new_url == "":
                new_video = save_url_video_form.save()
                video_title_kw = extract_video_kw(new_video)
                for kw in video_title_kw:
                    kw_db = KeyWord(key_word=kw)
                    kw_db.save()
                    new_video.key_word.add(kw_db)

            save_url_article_form = SaveArticleUrlForm(initial={"url_article": "Coller une url ici"})
            save_url_video_form = SaveVideoUrlForm(initial={"url_video": "Coller une url ici"})
            request_form = RequestForm()
            context = {'chatbot_pos': chatbot_pos, 'save_url_video_form': save_url_video_form,
                       'save_url_article_form': save_url_article_form, 'request_form': request_form}

            #print(Video.objects.filter(key_word__key_word__startswith="Python"))

            return render(request, 'chatbot/formulaire.html', context)


def get_content(key_word):

    list_of_content = []

    print(Video.objects.filter(key_word__key_word__startswith="Python"))


    return list_of_content
