from django.db import models


# Create your models here.

class KeyWord(models.Model):
    key_word = models.CharField(max_length=200)

    def __str__(self):
        return self.key_word


class Video(models.Model):
    url_video = models.URLField(max_length=200)
    key_word = models.ManyToManyField(KeyWord, max_length=300)

    def __str__(self):
        return self.url_video


class Article(models.Model):
    url_article = models.URLField(max_length=200)
    key_word = models.ManyToManyField(KeyWord, max_length=300, default='')

    def __str__(self):
        return self.url_article
